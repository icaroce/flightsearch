import 'package:flutter/material.dart';
import 'dart:math' as math;

class AnimatedHomeIcon extends AnimatedWidget {
  AnimatedHomeIcon({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {
    Animation<double> animation = super.listenable;
    return Icon(
      Icons.home,
      color: Colors.red,
      size: animation.value,
    );
  }
}
