class Flight {
  String from;
  String to;
  String date;
  String duration;
  String price;
  String fromToTime;
  DateTime departureDate;

  Flight(this.from, this.to, this.date, this.duration, this.price,
      this.fromToTime, this.departureDate);


}

class TicketFlightStop {
  String from;
  String fromShort;
  String to;
  String toShort;
  String flightNumber;

  TicketFlightStop(
      this.from, this.fromShort, this.to, this.toShort, this.flightNumber);
}
