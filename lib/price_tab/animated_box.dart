import 'package:flutter/material.dart';

class AnimatedBox extends AnimatedWidget {
  final Color color;
  static final double size = 21.0;

  AnimatedBox({
    Key key,
    Animation<double> animation,
    @required this.color,
  }) : super(key: key, listenable: animation);

  @override
  Widget build(BuildContext context) {
    Animation<double> animation = super.listenable;
    return Positioned(
      top: animation.value,
      child: Container(
          height: size,
          width: 50.0,
          decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.rectangle,
              border: Border.all(color: Color(0xFFDDDDDD), width: 0.5)),
          child: Padding(
            padding: const EdgeInsets.all(3.0),
            child: Text('01:00')
          )),
    );
  }
}
